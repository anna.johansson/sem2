# Mitt program

Dette programmet er en læringsplattform med to ulike læringsspill. Spillene er rettet mot barn i ung barneskolealder, og fokuserer på ord og grammatikk.

I det første spillet vil det dukke opp et lite ikon, eksempelvis en båt, en bil eller en sol. Under symbolet er det tre knapper med hvert sitt ord. Ett av tre ord beskriver ikonet. Dersom brukeren trykker på riktig knapp/ord kommer et nytt symbol opp.

Det andre spillet handler om at barna skal lære seg forskjellen mellom ulike ordklasser. I spillet kommer det opp et ord i midten av skjermen. Det er totalt fire ordklasser som vises på skjermen, én på hver side. Spilleren skal deretter bruke tiltastene for å indikere om ordet som har kommet opp er et substantiv, verb, preposisjon eller adjektiv.

Spilleren har totalt tre liv som vises i et panel på siden av skjermen. Ved feiltrykk mister spilleren et liv, og når alle livene er tapt går spillet til Game Over. Dersom spilleren velger riktig får spilleren 10 poeng. Antall poeng vises i en linje på bunnen av skjermen. Spillet går også i Game over når timeren har gått ut (1 minutt).

Her er link til forklaring på video:
https://youtu.be/9JIpXJ1cXTw
