package no.uib.inf101.sem2.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.Border;

import no.uib.inf101.sem2.model.GameState;

public class Klasse3Frame extends JFrame {

    GameState gameState = GameState.ACTIVE_GAME;

    private JLabel currentWord;
    private JLabel score;
    private JLabel xLabel1;
    private JLabel xLabel2;
    private JLabel xLabel3;
    private JLabel timeLabel;

    private Timer timer;
    private int timeLeft;

    /**
     * The Klasse3Frame constructor formats the klasse3 view by adding labels for the score, game, lives and time left.
     * Furthermore, it initializes the timer at 60 seconds.
     * 
     * @param word the random word (word, wordclass)
     */
    public Klasse3Frame(String word) {

        setSize(800, 400);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Colortheme
        Color lightBlue = new Color(51, 153, 255);
        Color lightPink = new Color(255, 182, 193);

        // Formatting of the heart/life panel

        ImageIcon heartOriginal = new ImageIcon("/Users/christinajohansson/Documents/INF101/sem2/src/main/java/no/uib/inf101/sem2/heart.png");
        Image originalImage = heartOriginal.getImage();
        Image scaledImage = originalImage.getScaledInstance(60, 50, Image.SCALE_SMOOTH);
        ImageIcon scaledHeart = new ImageIcon(scaledImage);

        xLabel1 = new JLabel(scaledHeart);
        xLabel2 = new JLabel(scaledHeart);
        xLabel3 = new JLabel(scaledHeart);

        JPanel heartsPanel = new JPanel();
        JLabel livLabel = new JLabel("Liv:");
        heartsPanel.add(livLabel);
        heartsPanel.add(xLabel1);
        heartsPanel.add(xLabel2);
        heartsPanel.add(xLabel3);
        heartsPanel.setBackground(Color.WHITE);


        // Formatting of the wordclass panel.

        JPanel wordClassPanel = new JPanel(new BorderLayout());

        JLabel labelNorth = new JLabel("Substantiv (trykk ⬆️)");
        JLabel labelSouth = new JLabel("Adjektiv (trykk ⬇️");
        JLabel labelEast = new JLabel("Verb (trykk ➡️)");
        JLabel labelWest = new JLabel("Preposisjon (trykk ⬅️");
        
        currentWord = new JLabel(word);
        currentWord.setHorizontalAlignment(JLabel.CENTER);
        currentWord.setVerticalAlignment(JLabel.CENTER);
        currentWord.setFont(new Font("SansSerif", Font.BOLD, 30));
        currentWord.setForeground(Color.BLACK);
        currentWord.setOpaque(true);
        currentWord.setBackground(lightPink);
        currentWord.setPreferredSize(new Dimension(5, 5));

        labelNorth.setHorizontalAlignment(JLabel.CENTER);
        labelSouth.setHorizontalAlignment(JLabel.CENTER);

        wordClassPanel.add(labelNorth, BorderLayout.NORTH);
        wordClassPanel.add(labelSouth, BorderLayout.SOUTH);
        wordClassPanel.add(labelEast, BorderLayout.EAST);
        wordClassPanel.add(labelWest, BorderLayout.WEST);
        wordClassPanel.add(currentWord);

        wordClassPanel.setBackground(lightPink);

        // Formatting for scoreboard panel

        JPanel scoreboardPanel = new JPanel();
        score = new JLabel("Score: 0");

        scoreboardPanel.add(score);
        scoreboardPanel.setBackground(lightBlue);
        scoreboardPanel.setPreferredSize(new Dimension(getWidth(), 50));

        // Formatting for Instructions and Time left panel

        timeLabel = new JLabel("Tid igjen: 60s");
        timeLabel.setForeground(Color.BLACK);
        timeLabel.setFont(new Font("SansSerif", Font.BOLD, 20));
        
        JLabel instructionLabel = new JLabel("Bruk piltastene til å plassere ordene i riktig ordklasse");
        instructionLabel.setForeground(Color.BLACK);
        instructionLabel.setFont(new Font("SansSerif", Font.BOLD, 15));
        
        JPanel timePanel = new JPanel(new BorderLayout());
        timePanel.add(timeLabel, BorderLayout.EAST);
        timePanel.add(instructionLabel, BorderLayout.WEST);
        timePanel.setBackground(lightBlue);
        timePanel.setPreferredSize(new Dimension(getWidth(), 50));


        // Adding all the panels to the frame

        add(wordClassPanel, BorderLayout.CENTER);
        add(scoreboardPanel, BorderLayout.SOUTH);
        add(heartsPanel, BorderLayout.EAST);
        add(timePanel, BorderLayout.NORTH);
    
        // Starting the timer
        timeLeft = 60;
        timer = new Timer(1000, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                timeLeft--;
                if (timeLeft >= 0) {
                    timeLabel.setText("Tid igjen " + timeLeft + "s");
                } else {
                    gameState = GameState.GAME_OVER;
                    ((Timer) e.getSource()).stop();
                    currentWord.setText("Tiden er ute!");
                }
            }
        });

        timer.start();
    }
    
    /**
     * The method sets the random word (currentword) to be displayed.
     * @param word the current word.
     */
    public void setWord(String word) {
        this.currentWord.setText(word);
    }

    /**
     * The method sets the score to be displayed.
     * @param score the current score.
     */
    public void setScore(int score) {
        this.score.setText("Score: " + score);
    }

    /**
     * The method displays the amount of lives left by using hearts and skulls. If there are no hearts left,
     * the gamestate becomes GAME_OVER. The method draws the skulls by using a png-file.
     * @param lives amount of lives left.
     */
    public void setLives(int lives) {

        ImageIcon skullOriginal = new ImageIcon("/Users/christinajohansson/Documents/INF101/sem2/src/main/java/no/uib/inf101/sem2/Skull.png");
        Image originalImage = skullOriginal.getImage();
        Image scaledImageSkull = originalImage.getScaledInstance(60, 50, Image.SCALE_SMOOTH);
        ImageIcon scaledSkull = new ImageIcon(scaledImageSkull);

        if (lives == 2) {
            xLabel1.setIcon(scaledSkull);

        } else if (lives == 1) {
            xLabel2.setIcon(scaledSkull);

        } else if (lives == 0) {
            xLabel3.setIcon(scaledSkull);
            timer.stop();
            gameState=GameState.GAME_OVER;
        }
    }

}
   
