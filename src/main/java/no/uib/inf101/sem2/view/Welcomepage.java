package no.uib.inf101.sem2.view;

import javax.swing.*;

import no.uib.inf101.sem2.controller.Klasse1Controller;
import no.uib.inf101.sem2.controller.Klasse3Controller;

import java.awt.*;
import java.awt.event.*;
import java.awt.Color;

public class Welcomepage extends JFrame implements ActionListener {
    
    private JButton button1, button2;
    
    /**
     * The constructor of the welcomePage handles the formatting of the welcomepage by adding buttons, text,
     * font and colors.
     */
    public Welcomepage() {
        super("Velkommen til dette læringsspillet!");
        
        // Create buttons
        button1 = new JButton("1. klasse");
        button2 = new JButton("3. klasse");

        // Is needed to show the background colors
        button1.setOpaque(true);
        button2.setOpaque(true);
        
        // Set the colors of the buttons
        button1.setBackground(new Color(255, 182, 193));
        button1.setForeground(Color.BLACK);
        button2.setBackground(new Color(51, 153, 255));
        button2.setForeground(Color.BLACK);

        // Set the font of the text
        Font font = new Font("Comic Sans", Font.BOLD, 25);
        button1.setFont(font);
        button2.setFont(font);

        // Set the colors and width of the borders of the buttons
        button1.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));
        button2.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));
        
        // Add action listeners
        button1.addActionListener(this);
        button2.addActionListener(this);

        // Set layout
        setLayout(new GridLayout(2, 1));
        
        // Add buttons to frame
        add(button1);
        add(button2);
        
        // Set frame properties
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 300);
        setLocationRelativeTo(null);
        setVisible(true);
    }
    
    /**
     * If the klasse 1 button is pressed, the welcomepage is closed and the klasse 1 frame opens. The same logic
     * applies to the klasse 3 button.
     */
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == button1) {
            dispose();
            new Klasse1Controller();
        }

        if (e.getSource() == button2) {
            dispose();
            new Klasse3Controller();
        }

    }
}
