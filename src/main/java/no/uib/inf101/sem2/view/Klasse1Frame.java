package no.uib.inf101.sem2.view;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import no.uib.inf101.sem2.controller.Klasse1Controller;
import no.uib.inf101.sem2.model.Word;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

public class Klasse1Frame extends JFrame implements ActionListener{

    private JButton answerButton1, answerButton2, answerButton3;
    private Word correctWord;

    /**
     * The Klasse1Frame constructor sets all of the formatting for the klasse 1 view/frame.
     * It adds two panels and three buttons. Furthermore it adds a label with an icon (the symbol of the word).
     * 
     * @param words the list of random words (word and symbol)
     */
    public Klasse1Frame(ArrayList<Word> words) {

        this.setSize(800, 400);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Split the frame in two panels. One for buttons with words, and one panel with the icon.
        JPanel buttonPanel = new JPanel();
        JPanel iconPanel = new JPanel();

        // The code beneath handles the buttonPanel.

        // add buttons with word options
        answerButton1 = new JButton(words.get(0).getWord());
        answerButton2 = new JButton(words.get(1).getWord());
        answerButton3 = new JButton(words.get(2).getWord());

        //add actions listeners
        answerButton1.addActionListener(this);
        answerButton2.addActionListener(this);
        answerButton3.addActionListener(this);

        // Formatting the buttons
        answerButton1.setOpaque(true);
        answerButton2.setOpaque(true);
        answerButton3.setOpaque(true);

        // Colortheme
        Color lightBlue = new Color(51, 153, 255);
        Color lightGreen = new Color(0, 255, 51);
        Color lightPink = new Color(255, 182, 193);
                
        // Set the colors of the buttons
        answerButton1.setBackground(lightBlue);
        answerButton1.setForeground(Color.BLACK);
        answerButton2.setBackground(lightPink);
        answerButton2.setForeground(Color.BLACK);
        answerButton3.setBackground(lightGreen);
        answerButton3.setForeground(Color.BLACK);
        
        // Set the font of the text
        Font font = new Font("Comic Sans", Font.BOLD, 25);
        answerButton1.setFont(font);
        answerButton2.setFont(font);
        answerButton3.setFont(font);

        // Set the borders of the answerbuttons
        answerButton1.setBorder(BorderFactory.createCompoundBorder());
        answerButton1.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));
        answerButton2.setBorder(BorderFactory.createCompoundBorder());
        answerButton2.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));
        answerButton3.setBorder(BorderFactory.createCompoundBorder());
        answerButton3.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));

        buttonPanel.setLayout(new GridLayout(1, 3));
        buttonPanel.add(answerButton1);
        buttonPanel.add(answerButton2);
        buttonPanel.add(answerButton3);

        // choose a random word to be correct
        Collections.shuffle(words);
        correctWord = words.get(0);

        // The code below draws everything in the iconpanel.

        // Add the symbol corresponding with the word, and create a new Jlabel with this symbol.
        String icon = correctWord.getSymbol();
        JLabel iconLabel = new JLabel(icon);

        // Center the label in the middle of the JPanel
        iconLabel.setHorizontalAlignment(JLabel.CENTER);
        iconLabel.setVerticalAlignment(JLabel.CENTER);

        // Place the icon in the center of the Label
        iconPanel.add(iconLabel, BorderLayout.CENTER);

        // Set the size of the icon as 50 pixlers bigger than original size
        Font fontIcon = iconLabel.getFont().deriveFont(Font.BOLD, 50f);
        iconLabel.setFont(fontIcon);

        // place icon in the upper middle part of screen
        Dimension sizeOfLabel = new Dimension(120, 120);
        iconLabel.setPreferredSize(sizeOfLabel);
        iconLabel.setBounds((800-120)/2, (200-120)/2, sizeOfLabel.width, sizeOfLabel.height);

        // Drawing a border for the Jlabel to surround the icon.
        Border border = BorderFactory.createLineBorder(Color.BLACK, 3);
        iconLabel.setBorder(border);

        iconPanel.setLayout(null);
        iconPanel.add(iconLabel);

        setLayout(new GridLayout(2, 1));

        add(iconPanel);
        add(buttonPanel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        Object b = e.getSource();

        if (b instanceof JButton) {
            if (correctWord.checkWord(((JButton) b).getText())) {
                System.out.println("Correct word clicked");
                dispose();
                new Klasse1Controller();
            }
        }
    }
  
}
