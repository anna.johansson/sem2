package no.uib.inf101.sem2.model;

public class Word {

    private String symbol;
    private String word;
    private WordClass wordClass;

    // Different constructors for different purposes

    /**
     * This is the constructor for class 3. It initializes the variables word and wordclass.
     * 
     * @param word A word randomly retrieved from a .txt file.
     * @param wordClass the type of the word retrieved.
     */
    public Word(String word, WordClass wordClass) {
        this.word = word;
        this.wordClass = wordClass;
    }

    /**
     * This is the constructor for class 1. Initializes the varibales symbol and word.
     * @param symbol an emoji.
     * @param word a word.
     */
    public Word(String symbol, String word) {
        this.symbol = symbol;
        this.word = word;
    }

    /**
     * The method retrieves the given symbol.
     * @return symbol, an emoji.
     */
    public String getSymbol() {
        return symbol;
    }

    /**
     * The method retrieves the given word.
     * @return a word.
     */
    public String getWord() {
        return word;
    }

    /**
     * The method retrieves the correct wordclass.
     * @return a wordclass, either noun, verb, adjective or preposistion.
     */
    public WordClass getWordClass() {
        return wordClass;
    }

    /**
     * This method checks if a given word equals the word retrieved.
     * 
     * @param word
     * @return true or false.
     */
    public boolean checkWord(String word) {
        return word.equals(this.word);
    }

}