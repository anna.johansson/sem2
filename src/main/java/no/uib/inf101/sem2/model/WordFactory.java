package no.uib.inf101.sem2.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class WordFactory {

    private static ArrayList<Word> all4WordClassesList = new ArrayList<>();

    private static final String NOUNS_FILE = "/Users/christinajohansson/Documents/INF101/sem2/src/main/java/no/uib/inf101/sem2/Substantiv.txt";
    private static final String VERBS_FILE = "/Users/christinajohansson/Documents/INF101/sem2/src/main/java/no/uib/inf101/sem2/Verb.txt";
    private static final String ADJECTIVES_FILE = "/Users/christinajohansson/Documents/INF101/sem2/src/main/java/no/uib/inf101/sem2/Adjektiv.txt";
    private static final String PREPOSITIONS_FILE = "/Users/christinajohansson/Documents/INF101/sem2/src/main/java/no/uib/inf101/sem2/Preposisjoner.txt";
    private static final Random random = new Random();

    private static ArrayList<Word> wordsAndSymbols = new ArrayList<>();

    private static final String WordList_FILE = "/Users/christinajohansson/Documents/INF101/sem2/src/main/java/no/uib/inf101/sem2/WordListKlasse1.txt";
    private static final String SymbolList_FILE = "/Users/christinajohansson/Documents/INF101/sem2/src/main/java/no/uib/inf101/sem2/SymbolListKlasse1.txt";
 
    
    // Method for retrieving words
    /**
     * This method reads through a file, where each word is on one line each. It combines the words to an arrayList
     * of strings.
     * @param fileName a .txt file
     * @return An arraylist with all of the words from the file read.
     * @throws IOException
     */
    public static ArrayList<String> readWordsFromFile(String fileName) throws IOException {
        ArrayList<String> wordListFromFile = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), StandardCharsets.UTF_8));
        String line = reader.readLine();
        while (line != null) {
            wordListFromFile.add(line.trim());
            line = reader.readLine();
        }
        reader.close();
        return wordListFromFile;
        }


        // Klasse 3
    
        /**
         * The get word class methods below reads through the correct file and picks a random word of the correct
         * word class.
         * @return a type word, with a word and a wordclass.
         * @throws IOException
         */
        public static Word getRandomNoun() throws IOException {
            ArrayList<String> words = readWordsFromFile(NOUNS_FILE);
            String randomWord = words.get(random.nextInt(words.size()));
            return new Word(randomWord, WordClass.SUBSTANTIV);
        }
    
        public static Word getRandomVerb() throws IOException {
            ArrayList<String> words = readWordsFromFile(VERBS_FILE);
            String randomWord = words.get(random.nextInt(words.size()));
            return new Word(randomWord, WordClass.VERB);
        }
    
        public static Word getRandomAdjective() throws IOException {
            ArrayList<String> words = readWordsFromFile(ADJECTIVES_FILE);
            String randomWord = words.get(random.nextInt(words.size()));
            return new Word(randomWord, WordClass.ADJEKTIV);
        }
    
        public static Word getRandomPreposition() throws IOException {
            ArrayList<String> words = readWordsFromFile(PREPOSITIONS_FILE);
            String randomWord = words.get(random.nextInt(words.size()));
            return new Word(randomWord, WordClass.PREPOSISJON);
        }
    
    
        /**
         * This method calls on each of the get random word class once, and adds a random word 
         * from each word class to a list.
         */
        private static void addWordsDifferentWordclasses() {
            try {
                all4WordClassesList.add(getRandomPreposition());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                all4WordClassesList.add(getRandomNoun());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                all4WordClassesList.add(getRandomAdjective());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                all4WordClassesList.add(getRandomVerb());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    
        /**
         * The method retrieves a random word from the list of random words from each word class. It does this by
         * using the Collections method in Java, shuffling the list and retrieving the word at index 0.
         */
        public static Word getRandomWord() {
            addWordsDifferentWordclasses();
            Collections.shuffle(all4WordClassesList);
            return all4WordClassesList.get(0);
        }


    // WordFactory for klasse 1

    /**
     * This method retrieves a random word of type word and symbol, by using the readwordsfromfile method. It then
     * picks a random index, and retrieves the word and symbol at given index.
     * 
     * @return returns a random word of type word and symbol.
     * @throws IOException
     */
    public static Word getRandomSymbolAndWord() throws IOException {
        ArrayList<String> words = readWordsFromFile(WordList_FILE);
        ArrayList<String> symbols = readWordsFromFile(SymbolList_FILE);

        Random random = new Random();
        int index = random.nextInt(words.size());

        String randomWord = words.get(index);
        String randomSymbol = symbols.get(index);

        return new Word(randomSymbol, randomWord);
    }

    /**
     * The method adds three random words and symbols to a list three times.
     */
    private static void addSymbolAndWord() {
        try {
            wordsAndSymbols.add(getRandomSymbolAndWord());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            wordsAndSymbols.add(getRandomSymbolAndWord());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            wordsAndSymbols.add(getRandomSymbolAndWord());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method calls on the addSymbolAndWord method to make a list of random words and symbols.
     * It shuffles the list and picks the word at index 0.
     * @param number amount of words from the list that should be retrieved.
     * @return a list of random words.
     */
    public static ArrayList<Word> getSymbolAndWord(int number) {
        addSymbolAndWord();
        Collections.shuffle(wordsAndSymbols);
        ArrayList<Word> randomWords = new ArrayList<>(wordsAndSymbols.subList(0, number));
        return randomWords;
    }
     
}
