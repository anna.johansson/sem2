package no.uib.inf101.sem2.model;

public class Score {
    private int points;
    private int lives;
    private int xCount;
    private int yCount;

    /**
     * The constructor Score initializes the variables points, lives, xCount (hearts) and yCount (skulls).
     * @param lives amount of lives left.
     */
    public Score(int lives) {
        points = 0;
        this.lives = lives;
        xCount = lives;
        yCount = 0;
    }

    /**
     * This method retrieves the amount of point the user has.
     * @return amount of points.
     */
    public int getPoints() {
        return points;
    }

    /**
     * The method adds points (10 per correct action) to the updated variable "points".
     * @param points the amount of points before the most recent correct action was made.
     */
    public void incrementScore(int points) {
        // Adds points
        this.points += points;
    }

    /**
     * Retrieves the amount of lives left.
     * @return Amount of lives left.
     */
    public int getLives() {
        return lives;
    }

    /**
     * This method substracts one life from the lives variable. If the amount of hearts (xcount) is above 0,
     * it substracts one heart and adds a skull.
     */
    public void loseOneLife() {
        lives--;
        if (xCount > 0) {
            xCount--;
            yCount++;
        }
    }

    /**
     * Counts the amount of hearts (xcount)
     * @return amount of hearts
     */
    public int getXCount() {
        return xCount;
    }

    /**
     * Counts the amount of skulls (ycount)
     * @return amount of skulls
     */
    public int getYCount() {
        return yCount;
    }
}

