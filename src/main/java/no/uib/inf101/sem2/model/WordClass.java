package no.uib.inf101.sem2.model;

public enum WordClass {
    SUBSTANTIV,
    ADJEKTIV,
    VERB,
    PREPOSISJON
}
