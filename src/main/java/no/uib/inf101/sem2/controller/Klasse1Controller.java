package no.uib.inf101.sem2.controller;

import java.util.ArrayList;

import no.uib.inf101.sem2.model.Word;
import no.uib.inf101.sem2.model.WordFactory;
import no.uib.inf101.sem2.view.Klasse1Frame;

public class Klasse1Controller {

    /** 
    * The constructor in Klasse1Controller makes a new list of 3 words and symbols, by calling the method from WordFactory.
    * Furthermore, it uses the list of 3 words when opening a new JFrame.
    */
    public Klasse1Controller() {

        ArrayList<Word> words = WordFactory.getSymbolAndWord(3);
        new Klasse1Frame(words);
    }
    
}
