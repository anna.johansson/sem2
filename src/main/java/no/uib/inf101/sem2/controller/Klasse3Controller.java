package no.uib.inf101.sem2.controller;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import no.uib.inf101.sem2.model.Score;
import no.uib.inf101.sem2.model.Word;
import no.uib.inf101.sem2.model.WordClass;
import no.uib.inf101.sem2.model.WordFactory;
import no.uib.inf101.sem2.view.Klasse3Frame;

public class Klasse3Controller {

    private Klasse3Frame frame;
    private Score score;
    private Word currentWord;
    private Word lastWord;
    private int lives = 3;

    /**
     * The constructer in Klasse3Controller initializes the currentWord variable 
     * by calling the getRandomWord method from WordFactory.
     * It then sets the last word to current word, which is later used to ensure that the same word does not appear twice.
     * The constructor also makes a new frame with the currentword.
     */
    public Klasse3Controller() {
        currentWord = WordFactory.getRandomWord();
        lastWord = currentWord;

        frame = new Klasse3Frame(currentWord.getWord());

        frame.addKeyListener(new Klasse3KeyListener());

        score = new Score(lives);

        // add scoreboard
        // when key pressed, logic is done
        // and scoreboard may be updated (in model and view)
    }

    /**
     * The correctKeyIsPressed method has several functions that are done when the correct key is pressed.
     * It firstly checks that the same word does not appear twice in a row. 
     * While currentword is the same as the lastWord, currentword is updated and a new random word 
     * is retrieved. Furthermore, the method sets the word and score when the user clicks on the right arrow.
     */
    private void correctKeyPressedAction() {
        // avoids picking the same word twice
        lastWord = currentWord;
        while (currentWord.getWord().equals(lastWord.getWord())) {
            currentWord = WordFactory.getRandomWord();
        }
        frame.setWord(currentWord.getWord());
        // Oppdaterer score
        score.incrementScore(10);
        // Oppdaterer feltet der scoren står
        frame.setScore(score.getPoints());
    }

    /**
     * The wrongKeyPressed method has several functions that are executed when the wrong key is pressed.
     * Firstly, it substracts one life. If the amount of lives left is less than 0, the game is over.
     * While there are more than 0 lives left, the user loses one life.
     */
    private void wrongKeyPressedAction() {
        lives--;
        if (lives < 0) {
            frame.setWord("Ingen flere liv igjen!");
            frame.removeKeyListener(frame.getKeyListeners()[0]);
        }
        else {
            score.loseOneLife();
            frame.setLives(score.getXCount());
        }
    }

    private class Klasse3KeyListener implements KeyListener {

        @Override
        public void keyPressed(KeyEvent e) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_UP:
                    if (currentWord.getWordClass() == WordClass.SUBSTANTIV) {
                        correctKeyPressedAction();
                    } else {
                        wrongKeyPressedAction();
                    }
                    break;

                case KeyEvent.VK_DOWN:
                    if (currentWord.getWordClass() == WordClass.ADJEKTIV) {
                        correctKeyPressedAction();
                    } else {
                        wrongKeyPressedAction();
                    }
                    break;

                case KeyEvent.VK_LEFT:
                    if (currentWord.getWordClass() == WordClass.PREPOSISJON) {
                        correctKeyPressedAction();
                    } else {
                        wrongKeyPressedAction();
                    }
                    break;

                case KeyEvent.VK_RIGHT:
                    if (currentWord.getWordClass() == WordClass.VERB) {
                        correctKeyPressedAction();
                    } else {
                        wrongKeyPressedAction();
                    }
                    break;

                default:
                    break;
            }
        }

        @Override
        public void keyReleased(KeyEvent arg0) {
            // do nothing
        }

        @Override
        public void keyTyped(KeyEvent arg0) {
            // do nothing
        }

    }
}